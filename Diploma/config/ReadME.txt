Methods:
1 - Exact Method
2 - Montecarlo Method
3 - Montecarlo parallel Method

Exact methods:
1 - Exact Test
2 - Exact Image
3 - Exact matrix
4 - Exact Diff Test
5 - Exact Diff Image
6 - Exact Diff Matrix
7 - Cum. updating of reliability bounds
8 - Cum. updating of reliability bounds matrix method

motecatlo methods:
std::cout << "Select Monte Carlo Method:" << std::endl;
std::cout << "1 - Reliability Test" << std::endl;
std::cout << "2 - Reliability Image" << std::endl;
std::cout << "3 - Reliability Matrix" << std::endl;
std::cout << "4 - Expected Reliability Test" << std::endl;
std::cout << "5 - Expected Reliability Image" << std::endl;
std::cout << "6 - Expected Reliability Matrix" << std::endl;