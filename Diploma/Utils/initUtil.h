#pragma once

void prepareOutputDir();

void prepareInputDir();

void prepareConfigDir();

void setConfig();

void selectInputFile();

int selectMethod();

void ompTest();

void expectedMethodInitWithArgs();

void monteCarloMethodInitWithArgs();

void monteCarloParallelMethodInitWithArgs();