#include "../Controllers/Methods/ExactMethod.h"
#include "../Controllers/Methods/MonteCarloMethod.h"
#include "../Controllers/Methods/Method.h"
#include "initUtil.h"

#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include "boost/filesystem.hpp"
#include <omp.h>

using namespace boost;

int accuracy, oImgSizeX, oImgSizeY, oImgScale, numThreads, method, methodCalculation;
float reliabilityFlag;
std::string oImgFormat;
std::string jsonFile;

void prepareOutputDir() {
	// ���������� ���������� �������� ������
	std::string _filePath = "output";
	const char* path = _filePath.c_str();
	boost::filesystem::path dir(path);
	if (boost::filesystem::remove_all(dir))
	{
	}
	if (boost::filesystem::create_directory(dir))
	{
	}
}

void prepareInputDir() {
	// ���������� ���������� ������� ������
	std::string _filePath = "input";
	const char* path = _filePath.c_str();
	boost::filesystem::path dir(path);
	if (boost::filesystem::create_directory(dir))
	{
	}
}

void prepareConfigDir() {
	// ���������� ���������� ��������
	std::string _filePath = "config";
	const char* path = _filePath.c_str();
	boost::filesystem::path dir(path);
	if (boost::filesystem::create_directory(dir))
	{
	}
}

void setThreadsNum() {
	if (numThreads <= omp_get_max_threads())
	{
		omp_set_num_threads(numThreads);
	}
	else
	{
		std::cout << "Provided number of threads is wrong, value is set to max available logical cores: " << omp_get_max_threads();
	}
}

void setConfig() {
	std::ifstream fin("config/config.txt");
	std::string line;
	std::istringstream sin;

	while (std::getline(fin, line))
	{
		sin.str(line.substr(line.find("=") + 1));
		if (line.find("accuracy") != std::string::npos)
		{
			std::cout << "Accuracy - " << sin.str() << std::endl;
			sin >> accuracy;
		}
		else if (line.find("oImgSizeX") != std::string::npos) 
		{
			std::cout << "Image size X - " << sin.str() << std::endl;
			sin >> oImgSizeX;
		}
		else if (line.find("oImgSizeY") != std::string::npos) 
		{
			std::cout << "Image size Y - " << sin.str() << std::endl;
			sin >> oImgSizeY;
		}
		else if (line.find("oImgScale") != std::string::npos) 
		{
			std::cout << "Image Scale - " << sin.str() << std::endl;
			sin >> oImgScale;
		}
		else if (line.find("oImgFormat") != std::string::npos) 
		{
			std::cout << "Image format - " << sin.str() << std::endl;
			sin >> oImgFormat;
		}
		else if (line.find("numThreads") != std::string::npos) 
		{
			std::cout << "Number of threads - " << sin.str() << std::endl;
			sin >> numThreads;
		}
		else if (line.find("jsonFile") != std::string::npos)
		{
			std::cout << "Json file - " << sin.str() << std::endl;
			sin >> jsonFile;
		}
		else if (line.find("methodSelection") != std::string::npos)
		{
			std::cout << "method - " << sin.str() << std::endl;
			sin >> method;
		}
		else if (line.find("methodCalculation") != std::string::npos)
		{
			std::cout << "Method Calculation - " << sin.str() << std::endl;
			sin >> methodCalculation;
		}
		else if (line.find("float reliabilityFlag") != std::string::npos)
		{
			std::cout << "float reliabilityFlag - " << sin.str() << std::endl;
			sin >> reliabilityFlag;
		}
		sin.clear();
	}

	setThreadsNum();
}

std::vector<std::string>  listInputFiles() {
	std::string _filePath = "input";
	const char* path = _filePath.c_str();

	std::vector<std::string> pathArray;
	int i = 0;
	for (auto& p : boost::filesystem::directory_iterator(path)) {
		std::cout << i << " - " << p << std::endl;

		// Convert path entry to string
		std::ostringstream oss;
		oss << p;
		std::string path_item = oss.str();

		// Unescape string
		path_item.erase(0, 1);
		path_item.erase(path_item.size() - 1);

		pathArray.push_back(path_item);
		i++;
	}

	return pathArray;
}

void selectInputFile() {
	int selectedFile = 0;

	//std::cout << "Select Input File:" << std::endl;
	//std::cin >> selectedFile;

	/*if ((selectedFile < 0) || (selectedFile > (pathArray.size() - 1))) {
		std::cout << "Wrong Input File!" << std::endl;
		selectInputFile();
	}*/

	INPUT_FILE_PATH = jsonFile;
	std::cout << "\nSelected Input FILE - " << INPUT_FILE_PATH << std::endl;
}

void exactInit(int methodId, float prFlag) {
	std::cout << "Exact Method - START" << endl;
	ExactMethod ExactMethod(accuracy, oImgSizeX, oImgSizeY, oImgScale, oImgFormat, reliabilityFlag);
	std::cout << "======= Method Init END =======" << endl;

	switch (methodId) {
	case 1:
		ExactMethod.recursiveTest();
		break;
	case 2:
		ExactMethod.recursiveImage();
		break;
	case 3:
		ExactMethod.recursiveMatrix();
		break;
	case 4:
		ExactMethod.recursiveDiffTest(prFlag);
		break;
	case 5:
		ExactMethod.recursiveDiffImage(prFlag);
		break;
	case 6:
		ExactMethod.recursiveDiffMatrix(prFlag);
		break;
	case 7:
		ExactMethod.CumRecursiveImage();
	case 8:
		ExactMethod.CumRecursiveMatrix();
	default:
		break;
	}

	std::cout << "Exact Method - END";
}

void montecarloInit(int methodId, int iterCount, float prFlag) {
	std::cout << "MonteCarlo Method - START" << std::endl;
	MonteCarloMethod MonteCarloMethod(accuracy, oImgSizeX, oImgSizeY, oImgScale, oImgFormat, reliabilityFlag);
	std::cout << "======= Method Init END =======" << std::endl;

	switch (methodId) {
	case 1:
		MonteCarloMethod.reliabilityTest(iterCount, prFlag);
		break;
	case 2:
		MonteCarloMethod.reliabilityImage(iterCount, prFlag);
		break;
	case 3:
		MonteCarloMethod.reliabilityMatrix(iterCount, prFlag);
		break;
	case 4:
		MonteCarloMethod.reliabilityExpectedTest(iterCount);
		break;
	case 5:
		MonteCarloMethod.reliabilityExpectedImage(iterCount);
		break;
	case 6:
		MonteCarloMethod.reliabilityExpectedMatrix(iterCount);
		break;
	default:
		break;
	}

	std::cout << "MonteCarlo Method - END";
}

void montecarloParallelInit(int methodId, int iterCount, float prFlag) {
	MonteCarloMethod MonteCarloMethod(accuracy, oImgSizeX, oImgSizeY, oImgScale, oImgFormat, reliabilityFlag);

	switch (methodId) {
	case 1:
		MonteCarloMethod.reliabilityParallelTest(iterCount, prFlag);
		break;
	case 2:
		MonteCarloMethod.reliabilityParallelImage(iterCount, prFlag);
		break;
	case 3:
		MonteCarloMethod.reliabilityParallelMatrix(iterCount, prFlag);
		break;
	case 4:
		MonteCarloMethod.reliabilityParallelExpectedTest(iterCount);
		break;
	case 5:
		MonteCarloMethod.reliabilityParallelExpectedImage(iterCount);
		break;
	case 6:
		MonteCarloMethod.reliabilityParallelExpectedMatrix(iterCount);
		break;
	default:
		break;
	}

	std::cout << "MonteCarlo Method - END";
}

int selectMethod() {
	/*int selectedMethod = 0;
	std::cout << "Select Method:" << std::endl;
	std::cout << "1 - Exact Method" << std::endl;
	std::cout << "2 - MonteCarlo Method" << std::endl;
	std::cout << "3 - MonteCarlo Parallel Method" << std::endl;
	std::cin >> selectedMethod;

	return selectedMethod;*/
	return method;
};

int selectExactMethod() {
	/*int selectedMethod = 0;
	std::cout << "Select Exact Method:" << std::endl;
	std::cout << "1 - Exact Test" << std::endl;
	std::cout << "3 - Exact Image" << std::endl;
	std::cout << "4 - Exact Matrix" << std::endl;
	std::cout << "5 - Exact Diff Test" << std::endl;
	std::cout << "6 - Exact Diff Image" << std::endl;
	std::cout << "7 - Exact Diff Matrix" << std::endl;
	std::cout << "8 - Cum. updating of reliability bounds" << std::endl;
	std::cin >> selectedMethod;

	return selectedMethod;*/
	return methodCalculation;
}

int selectMonteCarloMethod() {
	/*int selectedMethod = 0;
	std::cout << "Select Monte Carlo Method:" << std::endl;
	std::cout << "1 - Reliability Test" << std::endl;
	std::cout << "2 - Reliability Image" << std::endl;
	std::cout << "3 - Reliability Matrix" << std::endl;
	std::cout << "-----------------------" << std::endl;
	std::cout << "4 - Expected Reliability Test" << std::endl;
	std::cout << "5 - Expected Reliability Image" << std::endl;
	std::cout << "6 - Expected Reliability Matrix" << std::endl;
	std::cin >> selectedMethod;*/

	return methodCalculation;
}

int selectMonteCarloParallelMethod() {
	/*int selectedMethod = 0;
	std::cout << "Select Monte Carlo Parallel Method:" << std::endl;
	std::cout << "1 - Reliability Parallel Test" << std::endl;
	std::cout << "2 - Reliability Parallel Image" << std::endl;
	std::cout << "3 - Reliability Parallel Matrix" << std::endl;
	std::cout << "-----------------------" << std::endl;
	std::cout << "4 - Expected Reliability Parallel Test" << std::endl;
	std::cout << "5 - Expected Reliability Parallel Image" << std::endl;
	std::cout << "6 - Expected Reliability Parallel Matrix" << std::endl;
	std::cin >> selectedMethod;*/

	return methodCalculation;
}

float setProbabilityFlag() {
	float probabilityFlag = 0;
	std::cout << "Set Probability Flag(value should be between 0 & 1):" << std::endl;
	std::cin >> probabilityFlag;

	if ((probabilityFlag > 0) && (probabilityFlag < 1)) {
		return probabilityFlag;
	}
	else {
		std::cout << "Wrong Probability Flag, value should be between 0 & 1" << std::endl;
		setProbabilityFlag();
	}

	return probabilityFlag;
}

unsigned long setRealizationsCount() {
	unsigned long realizationsCount = 0;
	std::cout << "Set Realizations Count:" << std::endl;
	std::cin >> realizationsCount;

	if (realizationsCount > 0) {
		return realizationsCount;
	}
	else {
		std::cout << "Wrong Realizations Count, value should be > 0" << std::endl;
		setRealizationsCount();
	}

	return realizationsCount;
}

void expectedMethodInitWithArgs() {
expectedMethodSelect:
	int selectedMethod = selectExactMethod();
	switch (selectedMethod) {
	case 1:
		exactInit(1, 1);
		break;
	case 2:
		exactInit(2, 1);
		break;
	case 3:
		exactInit(3, 1);
		break;
	case 7:
		exactInit(7, 1);
	case 8:
		exactInit(8, 1);
	case 4:
		exactInit(4, setProbabilityFlag());
		break;
	case 5:
		exactInit(5, setProbabilityFlag());
		break;
	case 6:
		exactInit(6, setProbabilityFlag());
		break;
	default:
		std::cout << "Wrong Selected Method!" << std::endl;
		goto expectedMethodSelect;
		break;
	}
}

void monteCarloMethodInitWithArgs() {
monteCarloMethodSelect:
	int selectedMethod = selectMonteCarloMethod();
	switch (selectedMethod) {
	case 1:
		montecarloInit(1, setRealizationsCount(), setProbabilityFlag());
		break;
	case 2:
		montecarloInit(2, setRealizationsCount(), setProbabilityFlag());
		break;
	case 3:
		montecarloInit(3, setRealizationsCount(), setProbabilityFlag());
		break;
	case 4:
		montecarloInit(4, setRealizationsCount(), 1);
		break;
	case 5:
		montecarloInit(5, setRealizationsCount(), 1);
		break;
	case 6:
		montecarloInit(6, setRealizationsCount(), 1);
		break;
	default:
		std::cout << "Wrong Monte Carlo Method!" << std::endl;
		goto monteCarloMethodSelect;
		break;
	}
}

void monteCarloParallelMethodInitWithArgs() {
monteCarloParallelMethodSelect:
	int selectedMethod = selectMonteCarloParallelMethod();
	switch (selectedMethod) {
	case 1:
		montecarloParallelInit(1, setRealizationsCount(), setProbabilityFlag());
		break;
	case 2:
		montecarloParallelInit(2, setRealizationsCount(), setProbabilityFlag());
		break;
	case 3:
		montecarloParallelInit(3, setRealizationsCount(), setProbabilityFlag());
		break;
	case 4:
		montecarloParallelInit(4, setRealizationsCount(), 1);
		break;
	case 5:
		montecarloParallelInit(5, setRealizationsCount(), 1);
		break;
	case 6:
		montecarloParallelInit(6, setRealizationsCount(), 1);
		break;
	default:
		std::cout << "Wrong Monte Carlo Parallel Method!" << std::endl;
		goto monteCarloParallelMethodSelect;
		break;
	}
}

void ompTest() {
#pragma omp parallel
	printf("Hello from thread %d, nthreads %d\n", omp_get_thread_num(), omp_get_num_threads());
}