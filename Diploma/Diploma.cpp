﻿#include <iostream>
#include "boost/filesystem.hpp"
#include "Utils/initUtil.h"

int main()
{
    prepareOutputDir();
    prepareInputDir();
    prepareConfigDir();
    setConfig();

    selectInputFile();

selectMethod:
    int selectedMethod = selectMethod();
    switch (selectedMethod) {
    case 0:
        ompTest();
        break;
    case 1:
        expectedMethodInitWithArgs();
        break;
    case 2:
        monteCarloMethodInitWithArgs();
        break;
    case 3:
        monteCarloParallelMethodInitWithArgs();
        break;
    default:
        std::cout << "Wrong method.." << std::endl;
        goto selectMethod;
        break;
    }

    return EXIT_SUCCESS;
}