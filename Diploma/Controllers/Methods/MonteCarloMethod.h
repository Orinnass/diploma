#pragma once

#include "Method.h"

class MonteCarloMethod : protected Method {
public:
    MonteCarloMethod(unsigned int accuracy, unsigned int oImgSizeX, unsigned int oImgSizeY, unsigned int oImgScale,
        const string& oImgFormat, float reliabilityFlag);

    void reliabilityTest(unsigned long k, float f);

    void reliabilityImage(unsigned long k, float f);

    void reliabilityMatrix(unsigned long k, float f);

    void reliabilityExpectedTest(unsigned long k);

    void reliabilityExpectedImage(unsigned long k);

    void reliabilityExpectedMatrix(unsigned long k);

    /*
     * Parallel
     */

    void reliabilityParallelTest(unsigned long k, float f);

    void reliabilityParallelImage(unsigned long k, float f);

    void reliabilityParallelMatrix(unsigned long k, float f);

    void reliabilityParallelExpectedTest(unsigned long k);

    void reliabilityParallelExpectedImage(unsigned long k);

    void reliabilityParallelExpectedMatrix(unsigned long k);

private:
    unsigned long _kConnected;
    unsigned long _kProvided;
    float _coverageFlag;

    unsigned long getKConnected();

    void setKConnected(unsigned long kConnected);

    unsigned long getKProvided();

    void setKProvided(unsigned long kProvided);

    float getCoverageFlag();

    void setCoverageFlag(float coverageFlag);

    void init();

    float reliabilityMethodTest(vector<float> nodeRel);

    float reliabilityMethodImage(vector<float> nodeRel);

    float reliabilityMethodMatrix(vector<float> nodeRel);

    float reliabilityExpectedMethodTest(vector<float> nodeRel);

    float reliabilityExpectedMethodImage(vector<float> nodeRel);

    float reliabilityExpectedMethodMatrix(vector<float> nodeRel);

    /*
     * Parallel
     */

    float reliabilityParallelMethodTest(vector<float> nodeRel);

    float reliabilityParallelMethodImage(vector<float> nodeRel);

    float reliabilityParallelMethodMatrix(vector<float> nodeRel);

    float reliabilityParallelExpectedMethodTest(vector<float> nodeRel);

    float reliabilityParallelExpectedMethodImage(vector<float> nodeRel);

    float reliabilityParallelExpectedMethodMatrix(vector<float> nodeRel);
};