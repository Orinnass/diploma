#include <iostream>
#include <utility>
#include <algorithm>
#include <vector>
#include <string>

#include <cstdlib>
#include <ctime>

#include <omp.h>
#include "MonteCarloMethod.h"

using namespace boost;


MonteCarloMethod::MonteCarloMethod(unsigned int accuracy, unsigned int oImgSizeX, unsigned int oImgSizeY, unsigned int oImgScale,
	const string& oImgFormat, float reliabilityFlag) : Method(accuracy, oImgSizeX, oImgSizeY, oImgScale, oImgFormat, reliabilityFlag) {
	this->init();
}

void MonteCarloMethod::reliabilityTest(unsigned long k, float f) {
	this->setKProvided(k);
	this->setKConnected(0);
	this->setCoverageFlag(f);
	vector<float> prVector = this->getGraphProbabilities();
	std::cout << "MonteCarlo reliability (test) - START" << std::endl;
	float result = reliabilityMethodTest(prVector);
	std::cout << "WSN Network Reliability: " << result << std::endl;
	std::cout << "MonteCarlo reliability (test) - END" << std::endl;
}

void MonteCarloMethod::reliabilityImage(unsigned long k, float f) {
	this->setKProvided(k);
	this->setKConnected(0);
	this->setCoverageFlag(f);
	vector<float> prVector = this->getGraphProbabilities();
	std::cout << "MonteCarlo Reliability (image) - START" << std::endl;
	float result = reliabilityMethodImage(prVector);
	std::cout << "WSN Network Reliability: " << result << std::endl;
	std::cout << "MonteCarlo Reliability (image) - END" << std::endl;
}

void MonteCarloMethod::reliabilityMatrix(unsigned long k, float f) {
	this->setKProvided(k);
	this->setKConnected(0);
	this->setCoverageFlag(f);
	vector<float> prVector = this->getGraphProbabilities();
	std::cout << "MonteCarlo Reliability (matrix) - START" << std::endl;
	float result = reliabilityMethodMatrix(prVector);
	std::cout << "WSN Network Reliability: " << result << std::endl;
	std::cout << "MonteCarlo Reliability (matrix) - END" << std::endl;
}

void MonteCarloMethod::reliabilityExpectedTest(unsigned long k) {
	this->setKProvided(k);
	this->setKConnected(0);
	vector<float> prVector = this->getGraphProbabilities();
	std::cout << "MonteCarlo Expected Reliability (test) - START" << std::endl;
	float result = reliabilityExpectedMethodTest(prVector);
	std::cout << "WSN Network Reliability: " << result;
	std::cout << "MonteCarlo Expected Reliability (test) - END" << std::endl;
}

void MonteCarloMethod::reliabilityExpectedImage(unsigned long k) {
	this->setKProvided(k);
	this->setKConnected(0);
	vector<float> prVector = this->getGraphProbabilities();
	std::cout << "MonteCarlo Expected Reliability (image) - START" << std::endl;
	float result = reliabilityExpectedMethodImage(prVector);
	std::cout << "WSN Network Reliability: " << result << std::endl;
	std::cout << "MonteCarlo Expected Reliability (image) - END" << std::endl;
}

void MonteCarloMethod::reliabilityExpectedMatrix(unsigned long k) {
	this->setKProvided(k);
	this->setKConnected(0);
	vector<float> prVector = this->getGraphProbabilities();
	std::cout << "MonteCarlo Expected Reliability (matrix) - START" << std::endl;
	float result = reliabilityExpectedMethodMatrix(prVector);
	std::cout << "WSN Network Reliability: " << result << std::endl;
	std::cout << "MonteCarlo Expected Reliability (matrix) - END" << std::endl;
}

/*
 * Parallel
 */

void MonteCarloMethod::reliabilityParallelTest(unsigned long k, float f) {
	this->setKProvided(k);
	this->setKConnected(0);
	this->setCoverageFlag(f);
	vector<float> prVector = this->getGraphProbabilities();
	std::cout << "MonteCarlo Parallel Reliability (test) - START" << std::endl;
	float result = reliabilityParallelMethodTest(prVector);
	std::cout << "WSN Network Reliability: " << result << std::endl;
	std::cout << "MonteCarlo Parallel Reliability (test) - END" << std::endl;
}

void MonteCarloMethod::reliabilityParallelImage(unsigned long k, float f) {
	this->setKProvided(k);
	this->setKConnected(0);
	this->setCoverageFlag(f);
	vector<float> prVector = this->getGraphProbabilities();
	std::cout << "MonteCarlo Parallel Reliability (image) - START" << std::endl;
	float result = reliabilityParallelMethodImage(prVector);
	std::cout << "WSN Network Reliability: " << result << std::endl;
	std::cout << "MonteCarlo Parallel Reliability (image) - END" << std::endl;
}

void MonteCarloMethod::reliabilityParallelMatrix(unsigned long k, float f) {
	this->setKProvided(k);
	this->setKConnected(0);
	this->setCoverageFlag(f);
	vector<float> prVector = this->getGraphProbabilities();
	std::cout << "MonteCarlo Parallel Reliability (matrix) - START" << std::endl;
	float result = reliabilityParallelMethodMatrix(prVector);
	std::cout << "WSN Network Reliability: " << result << std::endl;
	std::cout << "MonteCarlo Parallel Reliability (matrix) - END" << std::endl;
}

void MonteCarloMethod::reliabilityParallelExpectedTest(unsigned long k) {
	this->setKProvided(k);
	this->setKConnected(0);
	vector<float> prVector = this->getGraphProbabilities();
	std::cout << "MonteCarlo Parallel Expected Reliability (test) - START" << std::endl;
	float result = reliabilityParallelExpectedMethodTest(prVector);
	std::cout << "WSN Network Reliability: " << result << std::endl;
	std::cout << "MonteCarlo Parallel Expected Reliability (test) - END" << std::endl;
}

void MonteCarloMethod::reliabilityParallelExpectedImage(unsigned long k) {
	this->setKProvided(k);
	this->setKConnected(0);
	vector<float> prVector = this->getGraphProbabilities();
	std::cout << "MonteCarlo Parallel Expected Reliability (image) - START" << std::endl;
	float result = reliabilityParallelExpectedMethodImage(prVector);
	std::cout << "WSN Network Reliability: " << result << std::endl;
	std::cout << "MonteCarlo Parallel Expected Reliability (image) - END" << std::endl;
}

void MonteCarloMethod::reliabilityParallelExpectedMatrix(unsigned long k) {
	this->setKProvided(k);
	this->setKConnected(0);
	vector<float> prVector = this->getGraphProbabilities();
	std::cout << "MonteCarlo Parallel Expected Reliability (matrix) - START" << std::endl;
	float result = reliabilityParallelExpectedMethodMatrix(prVector);
	std::cout << "WSN Network Reliability: " << result << std::endl;
	std::cout << "MonteCarlo Parallel Expected Reliability (matrix) - END" << std::endl;
}

unsigned long MonteCarloMethod::getKConnected() {
	return this->_kConnected;
}

void MonteCarloMethod::setKConnected(unsigned long kConnected) {
	this->_kConnected = kConnected;
}

unsigned long MonteCarloMethod::getKProvided() {
	return this->_kProvided;
}

void MonteCarloMethod::setKProvided(unsigned long kProvided) {
	this->_kProvided = kProvided;
}

float MonteCarloMethod::getCoverageFlag() {
	return this->_coverageFlag;
}

void MonteCarloMethod::setCoverageFlag(float coverageFlag) {
	this->_coverageFlag = coverageFlag;
}

void MonteCarloMethod::init() {
	this->graphInit(this->getGraphModel());

	this->fileItr = 0;
	this->setMaxCoverageInit();
	this->setKConnected(0);
	srand(static_cast <unsigned> (time(0)));// Seeding random number

	std::cout << "Monte-Carlo method - Initialized" << std::endl;
}

float MonteCarloMethod::reliabilityMethodTest(vector<float> nodeRel) {
	unsigned long kConn = this->getKConnected();
	vector<float> newRealization;

	for (unsigned long i = 0; i < this->getKProvided(); i++) {
		newRealization = nodeRel;
		for (unsigned long j = 1; j < nodeRel.size(); j++) {
			float r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
			newRealization.at(j) = (nodeRel.at(j) >= r) ? 1 : 0;
		}
		newRealization = this->updateGraphConnectivity(newRealization);
		if (this->countSquareTest(newRealization) >= this->getCoverageFlag()) this->setKConnected(kConn++);
	}

	float result = this->getKConnected();
	result /= this->getKProvided();

	return result;
}

float MonteCarloMethod::reliabilityMethodImage(vector<float> nodeRel) {
	unsigned long kConn = this->getKConnected();
	vector<float> newRealization;

	for (unsigned long i = 0; i < this->getKProvided(); i++) {
		newRealization = nodeRel;
		for (unsigned long j = 1; j < nodeRel.size(); j++) {
			float r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
			newRealization.at(j) = (nodeRel.at(j) >= r) ? 1 : 0;
		}
		newRealization = this->updateGraphConnectivity(newRealization);
		if (this->countSquare(newRealization) >= this->getCoverageFlag()) this->setKConnected(kConn++);
	}

	float result = this->getKConnected();
	result /= this->getKProvided();

	return result;
}

float MonteCarloMethod::reliabilityMethodMatrix(vector<float> nodeRel) {
	unsigned long kConn = this->getKConnected();
	vector<float> newRealization;

	for (unsigned long i = 0; i < this->getKProvided(); i++) {
		newRealization = nodeRel;
		for (unsigned long j = 1; j < nodeRel.size(); j++) {
			float r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
			newRealization.at(j) = (nodeRel.at(j) >= r) ? 1 : 0;
		}
		newRealization = this->updateGraphConnectivity(newRealization);
		if (this->countSquareMatrix(newRealization) >= this->getCoverageFlag()) this->setKConnected(kConn++);
	}

	float result = this->getKConnected();
	result /= this->getKProvided();

	return result;
}

float MonteCarloMethod::reliabilityExpectedMethodTest(vector<float> nodeRel) {
	float* kConnectedArr = new float[this->getKProvided()];
	vector<float> newRealization;

	for (unsigned long i = 0; i < this->getKProvided(); i++) {
		newRealization = nodeRel;
		for (unsigned long j = 1; j < nodeRel.size(); j++) {
			float r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
			newRealization.at(j) = (nodeRel.at(j) >= r) ? 1 : 0;
		}
		newRealization = this->updateGraphConnectivity(newRealization);
		kConnectedArr[i] = this->countSquareTest(newRealization);
	}

	float kConnected = 0, result = 0;
	for (unsigned long i = 0; i < this->getKProvided(); i++) {
		kConnected += kConnectedArr[i];
	}
	result += kConnected;
	result /= this->getKProvided();

	return result;
}

float MonteCarloMethod::reliabilityExpectedMethodImage(vector<float> nodeRel) {
	float* kConnectedArr = new float[this->getKProvided()];
	vector<float> newRealization;

	for (unsigned long i = 0; i < this->getKProvided(); i++) {
		newRealization = nodeRel;
		for (unsigned long j = 1; j < nodeRel.size(); j++) {
			float r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
			newRealization.at(j) = (nodeRel.at(j) >= r) ? 1 : 0;
		}
		newRealization = this->updateGraphConnectivity(newRealization);
		kConnectedArr[i] = this->countSquare(newRealization);
	}

	float kConnected = 0, result = 0;
	for (unsigned long i = 0; i < this->getKProvided(); i++) {
		kConnected += kConnectedArr[i];
	}
	result += kConnected;
	result /= this->getKProvided();

	return result;
}

float MonteCarloMethod::reliabilityExpectedMethodMatrix(vector<float> nodeRel) {
	float* kConnectedArr = new float[this->getKProvided()];
	vector<float> newRealization;

	for (unsigned long i = 0; i < this->getKProvided(); i++) {
		newRealization = nodeRel;
		for (unsigned long j = 1; j < nodeRel.size(); j++) {
			float r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
			newRealization.at(j) = (nodeRel.at(j) >= r) ? 1 : 0;
		}
		newRealization = this->updateGraphConnectivity(newRealization);
		kConnectedArr[i] = this->countSquareMatrix(newRealization);
	}

	float kConnected = 0, result = 0;
	for (unsigned long i = 0; i < this->getKProvided(); i++) {
		kConnected += kConnectedArr[i];
	}
	result += kConnected;
	result /= this->getKProvided();

	return result;
}

/*
 * Parallel
 */

float MonteCarloMethod::reliabilityParallelMethodTest(vector<float> nodeRel) {
	unsigned long kConn = this->getKConnected();
	vector<float> newRealization;

	unsigned long i, j;
#pragma omp parallel for private(j, newRealization)
	for (i = 0; i < this->getKProvided(); i++) {
		newRealization = nodeRel;
		for (j = 1; j < nodeRel.size(); j++) {
			float r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
			newRealization.at(j) = (nodeRel.at(j) >= r) ? 1 : 0;
		}
		newRealization = this->updateGraphConnectivity(newRealization);
		if (this->countSquareTest(newRealization) >= this->getCoverageFlag()) this->setKConnected(kConn++);
	}

	float result = this->getKConnected();
	result /= this->getKProvided();

	return result;
}

float MonteCarloMethod::reliabilityParallelMethodImage(vector<float> nodeRel) {
	unsigned long kConn = this->getKConnected();
	vector<float> newRealization;

	unsigned long i, j, itr = 0;
#pragma omp parallel for private(j, newRealization)
	for (i = 0; i < this->getKProvided(); i++) {
		newRealization = nodeRel;
		for (j = 1; j < nodeRel.size(); j++) {
			float r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
			newRealization.at(j) = (nodeRel.at(j) >= r) ? 1 : 0;
		}
		newRealization = this->updateGraphConnectivity(newRealization);
		if (this->countSquareParallel(newRealization, itr) >= this->getCoverageFlag()) this->setKConnected(kConn++);
		itr++;
	}

	float result = this->getKConnected();
	result /= this->getKProvided();

	return result;
}

float MonteCarloMethod::reliabilityParallelMethodMatrix(vector<float> nodeRel) {
	unsigned long kConn = this->getKConnected();
	vector<float> newRealization;

	unsigned long i, j;
#pragma omp parallel for private(j, newRealization)
	for (i = 0; i < this->getKProvided(); i++) {
		newRealization = nodeRel;
		for (j = 1; j < nodeRel.size(); j++) {
			float r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
			newRealization.at(j) = (nodeRel.at(j) >= r) ? 1 : 0;
		}
		newRealization = this->updateGraphConnectivity(newRealization);
		if (this->countSquareMatrix(newRealization) >= this->getCoverageFlag()) this->setKConnected(kConn++);
	}

	float result = this->getKConnected();
	result /= this->getKProvided();

	return result;
}

float MonteCarloMethod::reliabilityParallelExpectedMethodTest(vector<float> nodeRel) {
	float* kConnectedArr = new float[this->getKProvided()];
	vector<float> newRealization;

	unsigned long i, j;
#pragma omp parallel for private(j, newRealization)
	for (i = 0; i < this->getKProvided(); i++) {
		newRealization = nodeRel;
		for (j = 1; j < nodeRel.size(); j++) {
			float r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
			newRealization.at(j) = (nodeRel.at(j) >= r) ? 1 : 0;
		}
		newRealization = this->updateGraphConnectivity(newRealization);
		kConnectedArr[i] = this->countSquareTest(newRealization);
	}

	float kConnected = 0, result = 0;
	for (unsigned long i = 0; i < this->getKProvided(); i++) {
		kConnected += kConnectedArr[i];
	}
	result += kConnected;
	result /= this->getKProvided();

	return result;
}

float MonteCarloMethod::reliabilityParallelExpectedMethodImage(vector<float> nodeRel) {
	float* kConnectedArr = new float[this->getKProvided()];
	vector<float> newRealization;

	unsigned long i, j, itr = 0;
#pragma omp parallel for private(j, newRealization)
	for (i = 0; i < this->getKProvided(); i++) {
		newRealization = nodeRel;
		for (j = 1; j < nodeRel.size(); j++) {
			float r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
			newRealization.at(j) = (nodeRel.at(j) >= r) ? 1 : 0;
		}

		newRealization = this->updateGraphConnectivity(newRealization);
		kConnectedArr[i] = this->countSquareParallel(newRealization, itr);
		itr++;
	}

	float kConnected = 0, result = 0;
	for (unsigned long i = 0; i < this->getKProvided(); i++) {
		kConnected += kConnectedArr[i];
	}
	result += kConnected;
	result /= this->getKProvided();

	return result;
}

float MonteCarloMethod::reliabilityParallelExpectedMethodMatrix(vector<float> nodeRel) {
	float* kConnectedArr = new float[this->getKProvided()];
	vector<float> newRealization;

	unsigned long i, j;
#pragma omp parallel for private(j, newRealization)
	for (i = 0; i < this->getKProvided(); i++) {
		newRealization = nodeRel;
		for (j = 1; j < nodeRel.size(); j++) {
			float r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
			newRealization.at(j) = (nodeRel.at(j) >= r) ? 1 : 0;
		}

		newRealization = this->updateGraphConnectivity(newRealization);
		kConnectedArr[i] = this->countSquareMatrix(newRealization);
	}

	float kConnected = 0, result = 0;
	for (unsigned long i = 0; i < this->getKProvided(); i++) {
		kConnected += kConnectedArr[i];
	}
	result += kConnected;
	result /= this->getKProvided();

	return result;
}