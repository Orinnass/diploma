#include <iostream>
#include <utility>
#include <algorithm>
#include <vector>
#include <string>
#include <fstream>

#include "ExactMethod.h"

using namespace boost;


ExactMethod::ExactMethod(unsigned int accuracy, unsigned int oImgSizeX, unsigned int oImgSizeY, unsigned int oImgScale,
	const string& oImgFormat, float reliabilityFlag) : Method(accuracy, oImgSizeX, oImgSizeY, oImgScale, oImgFormat, reliabilityFlag)
{
	this->init();
}

void ExactMethod::recursiveTest() {
	vector<float> prVector = this->getGraphProbabilities();
	std::cout << "Exact Reliability (test) - START" << std::endl;
	float result_fix = recursiveMethodTest(prVector);
	std::cout << "WSN Network Reliability: " << result_fix << std::endl;
	std::cout << "Exact Reliability (test) - END" << std::endl;
}

void ExactMethod::recursiveImage() {
	vector<float> prVector = this->getGraphProbabilities();
	float result_fix = recursiveMethodImage(prVector);
}

void ExactMethod::CumRecursiveImage()
{
	vector<float> prVector = this->getGraphProbabilities();
	this->RL = 0;
	this->RU = this->getMaxCoverageFloat();
	float p = 1;
	this->i = 1;
	this->reliabilityFlag = false;
	std::ofstream RUfile;
	std::ofstream RLfile;

	RUfile.open("output/RU.txt");
	RLfile.open("output/RL.txt");

	RUfile << "( " << this->i << ", " << this->RU << " )" << endl;
	RLfile << "( " << this->i << ", " << this->RL << " )" << endl;

	RUfile.close();
	RLfile.close();

	float result_fix = CumUpdtrecursiveMethodImage(prVector, p);
	std::string answer;
	if (this->RU < this->threshold)
	{
		this->reliabilityFlag = false;
		answer = "unreliable";
	}
	if (this->RL >= this->threshold)
	{
		this->reliabilityFlag = true;
		answer = "reliable";
	}
}

void ExactMethod::CumRecursiveMatrix()
{
	this->setThreshold(this->setThreshold());

	vector<float> prVector = this->getGraphProbabilities();
	this->RL = 0;
	this->RU = this->getSquareMatrixMaxCoverageAgainstAll();
	float p = 1;
	this->i = 1;
	this->reliabilityFlag = false;
	std::ofstream RUfile;
	std::ofstream RLfile;

	RUfile.open("output/RU.txt");
	RLfile.open("output/RL.txt");

	RUfile << "( " << this->i << ", " << this->RU << " )" << endl;
	RLfile << "( " << this->i << ", " << this->RL << " )" << endl;

	RUfile.close();
	RLfile.close();

	std::cout << "Exact Reliability (matrix) - START" << endl;
	float result_fix = CumRecursiveMethodMatrix(prVector, p);
	std::string answer;
	if (this->RU < this->threshold)
	{
		this->reliabilityFlag = false;
		answer = "unreliable";
	}
	if (this->RL >= this->threshold)
	{
		this->reliabilityFlag = true;
		answer = "reliable";
	}
	std::cout << "WSN Network Reliability: " << answer << endl;
	std::cout << "Exact Reliability (matrix) - END" << endl;
}

void ExactMethod::recursiveMatrix() 
{
	this->setThreshold(this->setThreshold());

	vector<float> prVector = this->getGraphProbabilities();
	this->RL = 0;
	this->RU = this->getSquareMatrixMaxCoverageAgainstAll();
	float p = 1;
	this->i = 1;
	this->reliabilityFlag = false;
	std::ofstream RUfile;
	std::ofstream RLfile;

	RUfile.open("output/RU.txt");
	RLfile.open("output/RL.txt");

	RUfile << "( " << this->i << ", " << this->RU << " )" << endl;
	RLfile << "( " << this->i << ", " << this->RL << " )" << endl;

	RUfile.close();
	RLfile.close();

	std::cout << "Exact Reliability (matrix) - START" << endl;
	float result_fix = recursiveMethodMatrix(prVector);
	std::string answer;
	if (this->RU < this->threshold)
	{
		this->reliabilityFlag = false;
		answer = "unreliable";
	}
	if (this->RL >= this->threshold)
	{
		this->reliabilityFlag = true;
		answer = "reliable";
	}
	std::cout << "WSN Network Reliability: " << answer << endl;
	std::cout << "Exact Reliability (matrix) - END" << endl;
}

void ExactMethod::recursiveDiffTest(float coverageFlag) {
	this->setCoverageFlag(coverageFlag);
	vector<float> prVector = this->getGraphProbabilities();
	std::cout << "Exact Diff Reliability (test) - START";
	float result_fix = recursiveMethodTest(prVector);
	std::cout << "WSN Network Reliability: " << result_fix;
	std::cout << "Exact Diff Reliability (test) - END";
}

void ExactMethod::recursiveDiffImage(float coverageFlag) 
{
	this->setThreshold(this->setThreshold());
	this->setCoverageFlag(coverageFlag);
	vector<float> prVector = this->getGraphProbabilities();
	this->RL = 0;
	this->RU = 1;
	float p = 1;
	this->i = 1;
	this->reliabilityFlag = false;
	std::ofstream RUfile;
	std::ofstream RLfile;

	RUfile.open("output/RU.txt");
	RLfile.open("output/RL.txt");

	RUfile << "( " << this->i << ", " << this->RU << " )" << endl;
	RLfile << "( " << this->i << ", " << this->RL << " )" << endl;

	RUfile.close();
	RLfile.close();

	std::cout << "Exact Diff reliability (image) - START";
	float result_fix = recursiveDiffMethodImage(prVector, p);
	std::cout << "WSN Network Reliability: " << result_fix;
	std::cout << "Exact Diff reliability (image) - END";
}

void ExactMethod::recursiveDiffMatrix(float coverageFlag) {
	this->setCoverageFlag(coverageFlag);
	vector<float> prVector = this->getGraphProbabilities();
	std::cout << "Exact Diff reliability (matrix) - START";
	float result_fix = recursiveDiffMethodMatrix(prVector);
	std::cout << "WSN Network Reliability: " << result_fix;
	std::cout << "Exact Diff reliability (matrix) - END";
}

float ExactMethod::getCoverageFlag() {
	return this->_coverageFlag;
}

void ExactMethod::setCoverageFlag(float coverageFlag) {
	this->_coverageFlag = coverageFlag;
}

void ExactMethod::init() {
	this->graphInit(this->getGraphModel());

	this->fileItr = 0;
	this->setMaxCoverageInit();

	std::cout << "Exact method - Initialized" << endl;
}

float ExactMethod::recursiveMethodImage(vector<float> nodeRel)
{
	float result = 0;
	unsigned int v = 0;

	for (unsigned int i = 0; i < this->_graphModel.getNodes().size(); i++) {
		if (nodeRel.at(i) == 1) {
			for (unsigned int neighborVertexId : this->_graphModel.getNodes().at(i).getRelations()) {
				float pr = nodeRel.at(neighborVertexId);
				if ((nodeRel.at(neighborVertexId) > 0) && (nodeRel.at(neighborVertexId) < 1)) {
					v = neighborVertexId;
					break;
				}
			}
		}
		if (v > 0) break;
	}

	if (v > 0) {
		nodeRel.at(v) = 1;
		result = this->_graphModel.getNodes().at(v).getReliablility() * recursiveMethodImage(nodeRel);
		nodeRel.at(v) = 0;
		result += (1 - this->_graphModel.getNodes().at(v).getReliablility()) * recursiveMethodImage(nodeRel);
	}
	else if (v == 0) result = this->countSquare(nodeRel);

	return result;
}

float ExactMethod::CumUpdtrecursiveMethodImage(vector<float> nodeRel, float p)
{
	if (this->threshold != 0)
	{
		if (this->RU < this->threshold)
		{
			this->reliabilityFlag = false;
			return 0;
		}
	}

	float result = 0;
	unsigned int v = 0;

	for (unsigned int i = 0; i < this->_graphModel.getNodes().size(); i++) {
		if (nodeRel.at(i) == 1) {
			for (unsigned int neighborVertexId : this->_graphModel.getNodes().at(i).getRelations()) {
				float pr = nodeRel.at(neighborVertexId);
				if ((nodeRel.at(neighborVertexId) > 0) && (nodeRel.at(neighborVertexId) < 1)) {
					v = neighborVertexId;
					break;
				}
			}
		}
		if (v > 0) break;
	}

	if (v > 0) 
	{
		float v_rel = nodeRel.at(v);
		nodeRel.at(v) = 1;
		result = this->_graphModel.getNodes().at(v).getReliablility() * CumUpdtrecursiveMethodImage(nodeRel, p * v_rel);
		nodeRel.at(v) = 0;
		result += (1 - this->_graphModel.getNodes().at(v).getReliablility()) * CumUpdtrecursiveMethodImage(nodeRel, p * (1 - v_rel));
	}
	else if (v == 0)
	{
		result = this->countSquare(nodeRel);
		float cs = result;

		this->RL = this->RL + p * cs;
		this->RU = this->RU - p * (this->getMaxCoverageFloat() - cs);

		std::ofstream RUfile;
		std::ofstream RLfile;
		this->i++;
		RUfile.open("output/RU.txt", std::ios_base::app);
		RLfile.open("output/RL.txt", std::ios_base::app);

		RUfile << "( " << this->i << ", " << this->RU << " )" << endl;
		RLfile << "( " << this->i << ", " << this->RL << " )" << endl;

		RUfile.close();
		RLfile.close();
	}

	return result;
}

float ExactMethod::recursiveMethodTest(vector<float> nodeRel) {
	float result = 0;
	unsigned int v = 0;

	for (unsigned int i = 0; i < this->_graphModel.getNodes().size(); i++) {
		if (nodeRel.at(i) == 1) {
			for (unsigned int neighborVertexId : this->_graphModel.getNodes().at(i).getRelations()) {
				float pr = nodeRel.at(neighborVertexId);
				if ((nodeRel.at(neighborVertexId) > 0) && (nodeRel.at(neighborVertexId) < 1)) {
					v = neighborVertexId;
					break;
				}
			}
		}
		if (v > 0) break;
	}

	if (v > 0) {
		nodeRel.at(v) = 1;
		result = this->_graphModel.getNodes().at(v).getReliablility() * recursiveMethodTest(nodeRel);
		nodeRel.at(v) = 0;
		result += (1 - this->_graphModel.getNodes().at(v).getReliablility()) * recursiveMethodTest(nodeRel);
	}
	else if (v == 0) result = this->countSquareTest(nodeRel);

	return result;
}

float ExactMethod::recursiveMethodMatrix(vector<float> nodeRel) 
{
	float result = 0;
	unsigned int v = 0;

	for (unsigned int i = 0; i < this->_graphModel.getNodes().size(); i++) 
	{
		if (nodeRel.at(i) == 1) 
		{
			for (unsigned int neighborVertexId : this->_graphModel.getNodes().at(i).getRelations()) 
			{
				float pr = nodeRel.at(neighborVertexId);
				if ((nodeRel.at(neighborVertexId) > 0) && (nodeRel.at(neighborVertexId) < 1)) 
				{
					v = neighborVertexId;
					break;
				}
			}
		}
		if (v > 0) break;
	}

	if (v > 0) {
		float v_rel = nodeRel.at(v);
		nodeRel.at(v) = 1;
		result = this->_graphModel.getNodes().at(v).getReliablility() * recursiveMethodMatrix(nodeRel);
		nodeRel.at(v) = 0;
		result += (1 - this->_graphModel.getNodes().at(v).getReliablility()) * recursiveMethodMatrix(nodeRel);
	}

	return result;
}

float ExactMethod::CumRecursiveMethodMatrix(vector<float> nodeRel, float p)
{
	if (this->threshold != 0)
	{
		if (this->RU < this->threshold)
		{
			this->reliabilityFlag = false;
			return 0;
		}
	}

	float result = 0;
	unsigned int v = 0;

	for (unsigned int i = 0; i < this->_graphModel.getNodes().size(); i++)
	{
		if (nodeRel.at(i) == 1)
		{
			for (unsigned int neighborVertexId : this->_graphModel.getNodes().at(i).getRelations())
			{
				float pr = nodeRel.at(neighborVertexId);
				if ((nodeRel.at(neighborVertexId) > 0) && (nodeRel.at(neighborVertexId) < 1))
				{
					v = neighborVertexId;
					break;
				}
			}
		}
		if (v > 0) break;
	}

	if (v > 0) {
		float v_rel = nodeRel.at(v);
		nodeRel.at(v) = 1;
		result = this->_graphModel.getNodes().at(v).getReliablility() * CumRecursiveMethodMatrix(nodeRel, p * v_rel);
		nodeRel.at(v) = 0;
		result += (1 - this->_graphModel.getNodes().at(v).getReliablility()) * CumRecursiveMethodMatrix(nodeRel, p * (1 - v_rel));
	}
	else if (v == 0)
	{
		result = this->countSquareMatrix(nodeRel);

		float cs = result;

		this->RL = this->RL + p * cs;
		this->RU = this->RU - p * (this->getSquareMatrixMaxCoverageAgainstAll() - cs);

		std::ofstream RUfile;
		std::ofstream RLfile;
		this->i++;
		RUfile.open("output/RU.txt", std::ios_base::app);
		RLfile.open("output/RL.txt", std::ios_base::app);

		RUfile << "( " << this->i << ", " << this->RU << " )" << endl;
		RLfile << "( " << this->i << ", " << this->RL << " )" << endl;

		RUfile.close();
		RLfile.close();
	}

	return result;
}

float ExactMethod::recursiveDiffMethodTest(vector<float> nodeRel) {
	float result = 0;
	unsigned int v = 0;

	if (!(this->countSquareTest(nodeRel) > this->getCoverageFlag())) {
		for (unsigned int i = 0; i < this->_graphModel.getNodes().size(); i++) {
			if (nodeRel.at(i) == 1) {
				for (unsigned int neighborVertexId : this->_graphModel.getNodes().at(i).getRelations()) {
					float pr = nodeRel.at(neighborVertexId);
					if ((nodeRel.at(neighborVertexId) > 0) && (nodeRel.at(neighborVertexId) < 1)) {
						v = neighborVertexId;
						break;
					}
				}
			}
			if (v > 0) break;
		}

		if (v > 0) {
			nodeRel.at(v) = 1;
			result = this->_graphModel.getNodes().at(v).getReliablility() * recursiveDiffMethodTest(nodeRel);
			nodeRel.at(v) = 0;
			result += (1 - this->_graphModel.getNodes().at(v).getReliablility()) * recursiveDiffMethodTest(nodeRel);
		}
		else if (v == 0) result = this->countSquareTest(nodeRel);
	}
	else {
		result = 1;
	}

	return result;
}

float ExactMethod::recursiveDiffMethodImage(vector<float> nodeRel, float p) 
{
	float result = 0;
	unsigned int v = 0;

	float sq = this->countSquare(nodeRel);

	if (sq > this->getCoverageFlag()) 
	{
		result = 1;
		this->RL += p;
	}
	else 
	{
		for (unsigned int i = 0; i < this->_graphModel.getNodes().size(); i++) 
		{
			if (nodeRel.at(i) == 1) {
				for (unsigned int neighborVertexId : this->_graphModel.getNodes().at(i).getRelations()) 
				{
					float pr = nodeRel.at(neighborVertexId);
					if ((nodeRel.at(neighborVertexId) > 0) && (nodeRel.at(neighborVertexId) < 1)) 
					{
						v = neighborVertexId;
						break;
					}
				}
			}
			if (v > 0)
			{
				break;
			}
		}

		if (v > 0) 
		{
			float v_rel = nodeRel.at(v);
			nodeRel.at(v) = 1;
			result = this->_graphModel.getNodes().at(v).getReliablility() * recursiveDiffMethodImage(nodeRel, p * v_rel);
			nodeRel.at(v) = 0;
			result += (1 - this->_graphModel.getNodes().at(v).getReliablility()) * recursiveDiffMethodImage(nodeRel, p * (1 - v_rel));
		}
		else
		{
			this->RU -= p;
			std::ofstream RUfile;
			std::ofstream RLfile;
			this->i++;
			RUfile.open("output/RU.txt", std::ios_base::app);
			RLfile.open("output/RL.txt", std::ios_base::app);

			RUfile << "( " << this->i << ", " << this->RU << " )" << endl;
			RLfile << "( " << this->i << ", " << this->RL << " )" << endl;

			RUfile.close();
			RLfile.close();
		}
	}

	return result;
}

float ExactMethod::recursiveDiffMethodMatrix(vector<float> nodeRel) {
	float result = 0;
	unsigned int v = 0;

	float sq = this->countSquareMatrix(nodeRel);

	if (sq > this->getCoverageFlag()) {
		result = 1;
	}
	else {
		for (unsigned int i = 0; i < this->_graphModel.getNodes().size(); i++) {
			if (nodeRel.at(i) == 1) {
				for (unsigned int neighborVertexId : this->_graphModel.getNodes().at(i).getRelations()) {
					float pr = nodeRel.at(neighborVertexId);
					if ((nodeRel.at(neighborVertexId) > 0) && (nodeRel.at(neighborVertexId) < 1)) {
						v = neighborVertexId;
						break;
					}
				}
			}
			if (v > 0) break;
		}

		if (v > 0) {
			nodeRel.at(v) = 1;
			result = this->_graphModel.getNodes().at(v).getReliablility() * recursiveDiffMethodMatrix(nodeRel);
			nodeRel.at(v) = 0;
			result += (1 - this->_graphModel.getNodes().at(v).getReliablility()) * recursiveDiffMethodMatrix(nodeRel);
		}
	}

	return result;
}