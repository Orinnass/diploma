#pragma once
#include "Method.h"

class ExactMethod : protected Method
{
public:
    ExactMethod(unsigned int accuracy, unsigned int oImgSizeX, unsigned int oImgSizeY, unsigned int oImgScale, 
        const string& oImgFormat, float reliabilityFlag);

    void recursiveTest();

    void recursiveImage();

    void recursiveMatrix();

    void CumRecursiveImage();

    void CumRecursiveMatrix();

    void recursiveDiffTest(float coverageFlag);

    void recursiveDiffImage(float coverageFlag);

    void recursiveDiffMatrix(float coverageFlag);

private:
    float _coverageFlag;
    float RL;
    float RU;
    float square;
    bool firstGen;

    float getCoverageFlag();

    void setCoverageFlag(float coverageFlag);

    void init();

    float recursiveMethodImage(vector<float> nodeRel);

    float CumUpdtrecursiveMethodImage(vector<float> nodeRel, float p);

    float recursiveMethodTest(vector<float> nodeRel);

    float recursiveMethodMatrix(vector<float> nodeRel);

    float recursiveDiffMethodTest(vector<float> nodeRel);

    float recursiveDiffMethodImage(vector<float> nodeRel, float p);

    float recursiveDiffMethodMatrix(vector<float> nodeRel);
    
    float CumRecursiveMethodMatrix(vector<float> nodeRel, float p);
};