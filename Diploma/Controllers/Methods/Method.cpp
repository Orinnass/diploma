#include <iostream>
#include <utility>
#include <algorithm>
#include <vector>
#include <string>

#include "boost/graph/graph_traits.hpp"
#include "boost/graph/adjacency_list.hpp"
#include "boost/graph/breadth_first_search.hpp"
#include "boost/graph/depth_first_search.hpp"
#include "boost/graph/graphviz.hpp"
#include "boost/algorithm/string.hpp"

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/types_c.h>

std::string INPUT_FILE_PATH("input/graph_input.json");//("input/graph_input.json");

#include "../../Models/Graph.h"
#include "Method.h"

using namespace boost;

template <typename Vertex, typename Graph> void custom_bfs_visitor::discover_vertex(Vertex v, const Graph& g)
{
	std::cout << "Visited vertex - " << v;
}


void custom_dfs_visitor::discover_vertex(vertex_t v, const graph_t& g)
{
	std::cout << "Visited vertex - " << v;
}


// Graphviz property writer (vertex)

custom_vertex_writer::custom_vertex_writer(graph_t& g, Graph& gModel) : currentGraph(g), currentGraphModel(gModel) {}

template <class VertexOrEdge>
void custom_vertex_writer::operator()(std::ostream& out, const VertexOrEdge& e)
{
	float coverage = this->currentGraphModel.getNodes().at(e).getCoverage();
	float posX = this->currentGraphModel.getNodes().at(e).getCoordinates().at(0);
	float posY = this->currentGraphModel.getNodes().at(e).getCoordinates().at(1);
	if (e != 0) {
		out << "[shape=circle,width=" << coverage
			<< ",style=filled,fillcolor=\"#000000\",pos=\""
			<< posX << "," << posY << "!\", pin=true]";
	}
	else {
		out << "[shape=circle,width=" << coverage
			<< ",style=invis,pos=\""
			<< posX << "," << posY << "!\", pin=true]";
	}

}


// Graphviz property writer (edge)
custom_edge_writer::custom_edge_writer(graph_t& g) : currentGraph(g) {}

template <class VertexOrEdge>
void custom_edge_writer::operator()(std::ostream& out, const VertexOrEdge& e)
{
	out << "[style=invis]";
}


Method::Method(unsigned int accuracy, unsigned int oImgSizeX, unsigned int oImgSizeY, unsigned int oImgScale, string oImgFormat, float reliabilityFlag)
	: _accuracy(accuracy),
	_oImgSizeX(oImgSizeX),
	_oImgSizeY(oImgSizeY),
	_oImgCoordScale(oImgScale),
	_oImgFormat(oImgFormat),
	_graphModel(Graph::initWithFile(INPUT_FILE_PATH.c_str()))
{
	std::cout << "Method - Initialized" << std::endl;
	setThreshold(reliabilityFlag);
	this->init();
}

void Method::init()
{
	this->graphInit(this->getGraphModel());

	std::cout << "Boost BFS - START" << std::endl;
	this->boost_bfs();
	std::cout << "Boost BFS - END" << std::endl;

	std::cout << "Boost DFS - START" << std::endl;
	this->boost_dfs();
	std::cout << "Boost DFS - END" << std::endl;


	this->fileItr = 0;
	this->setMaxCoverageInit();

	std::cout << "Method - Initialized" << std::endl;
}

unsigned int Method::getAccuracy()
{
	return this->_accuracy;
}

void Method::setAccuracy(unsigned int accuracy)
{
	this->_accuracy = accuracy;
}

bool Method::getReliabilityFlag()
{
	return this->reliabilityFlag;
}

void Method::setReliabilityFlag(bool value)
{
	this->reliabilityFlag = value;
}

void Method::setThreshold(float value)
{
	this->threshold = value;
}

float Method::getThreshold()
{
	return this->threshold;
}

float Method::setThreshold()
{
	float flag;

	std::cout << "Set Reliability Flag (value should be between 0 & 1)" << endl;
	std::cin >> flag;

	if ((flag >= 0) && (flag <= 1)) 
	{
		return flag;
	}
	else 
	{
		std::cout << "Wrong Reliability Flag, value should be between 0 & 1" << std::endl;
		setThreshold();
	}

	return flag;
}

std::vector<Edge> Method::getEdgeVector()
{
	return this->_edgeVector;
}

void Method::setEdgeVector(std::vector<Edge> edgeVector)
{
	this->_edgeVector = edgeVector;
}

graph_t Method::getUndirectedGraph()
{
	return this->_graph_t;
}

void Method::setUndirectedGraph(graph_t undGraph)
{
	this->_graph_t = undGraph;
}

Graph Method::getGraphModel()
{
	return this->_graphModel;
}

void Method::setGraphModel(Graph graphModel)
{
	this->_graphModel = graphModel;
}

unsigned long Method::getMaxCoverage()
{
	return this->_maxCoverage;
}

void Method::setMaxCoverage(unsigned long maxCoverage)
{
	this->_maxCoverage = maxCoverage;
}

unsigned long Method::getMaxCoverageMatrix()
{
	return this->_maxCoverageMatrix;
}

void Method::setMaxCoverageMatrix(unsigned long maxCoverageMatrix)
{
	this->_maxCoverageMatrix = maxCoverageMatrix;
}

std::vector<float> Method::getVisitedNodes()
{
	return this->_visited;
}

void Method::setVisitedNodes(std::vector<float> visited)
{
	this->_visited = visited;
}

void Method::setMaxCoverageFloat(float maxCoverage)
{
	this->_maxCoverageFloat = maxCoverage;
}

float Method::getMaxCoverageFloat()
{
	return this->_maxCoverageFloat;
}

float Method::getSquareMatrixMaxCoverageAgainstAll()
{
	return this->squareMatrixMaxCoverageAgainstAll;
}

void Method::setSquareMatrixMaxCoverageAgainstAll(float value)
{
	this->squareMatrixMaxCoverageAgainstAll = value;
}

void Method::graphInit(Graph graphModel)
{
	std::cout << "Adding edges to graph - START" << std::endl;
	std::vector<Edge> edgeVec;
	for (Node node : graphModel.getNodes()) {
		for (unsigned int neighborVertex : node.getRelations()) {
			edgeVec.push_back(Edge(node.getId(), neighborVertex));
		}
	}
	this->setEdgeVector(edgeVec);
	std::cout << "Adding edges to graph - END" << std::endl;

	std::cout << "Initializing graph with edges - START" << std::endl;
	// GraphInit
	graph_t g(edgeVec.begin(), edgeVec.end(), graphModel.getNodes().size());

	graph_traits<graph_t>::vertex_iterator vi, vi_end, next;
	tie(vi, vi_end) = vertices(g);
	for (next = vi; vi != vi_end; vi = next) {
		++next;
	}

	this->setUndirectedGraph(g);
	std::cout << "Edges Num - " << num_edges(g) << std::endl;
	std::cout << "Initializing graph with edges - END" << std::endl;

	// List all edges
	typedef graph_traits<graph_t>::edge_iterator edge_iterator;
	std::pair<edge_iterator, edge_iterator> ei = edges(g);
	for (edge_iterator edge_iter = ei.first; edge_iter != ei.second; ++edge_iter) {
		std::cout << "Edge (" << source(*edge_iter, g) << ", " << target(*edge_iter, g) << ")";
	}
}

void Method::setMaxCoverageInit()
{
	std::string maxCoveragePath = "_max_coverage";
	this->graphToImg(maxCoveragePath, this->_graph_t);
	this->setMaxCoverage(this->maxCoverageReadImg(maxCoveragePath)); // Count of black pixels for graph with max coverage
	std::cout << "Ratio of black pixels against all for graph with max coverage (image) - "
		<< this->maxCoverageReadImgRatioAgainstAll(maxCoveragePath) << std::endl;

	std::vector<float> visited;
	std::vector<float> prVector = this->getGraphProbabilities();
	visited = this->updateGraphConnectivity(prVector);
	this->setMaxCoverageMatrix(this->countSquareMatrixMaxCoverage(visited)); // Count of black pixels for graph with max coverage
	this->setSquareMatrixMaxCoverageAgainstAll(this->countSquareMatrixMaxCoverageAgainstAll(visited));
	std::cout << "Ratio of black pixels against all for graph with max coverage (matrix) - "
		<< this->getSquareMatrixMaxCoverageAgainstAll() << std::endl;
}

std::vector<float> Method::getGraphProbabilities()
{
	Graph g = this->_graphModel;
	std::vector<float> prVector;
	for (Node node : g.getNodes()) {
		prVector.push_back(node.getReliablility());
	}
	return prVector;
}

// Return set of visited vertices in connected graph
void Method::recursiveVertexVisit(std::vector<float> nodeRel)
{
	unsigned int v = 0;

	for (unsigned int i = 0; i < nodeRel.size(); i++) {
		if (nodeRel.at(i) == 1) {
			for (unsigned int neighborVertexId : this->_graphModel.getNodes().at(i).getRelations()) {
				float visitedV = this->getVisitedNodes().at(neighborVertexId);
				if ((nodeRel.at(neighborVertexId) > 0) && (visitedV != 1)) {
					v = neighborVertexId;
					break;
				}
			}
		}
		if (v > 0) break;
	}

	if (v > 0) {
		std::vector<float> visited = this->getVisitedNodes();
		visited.at(v) = 1;
		this->setVisitedNodes(visited);
		this->recursiveVertexVisit(nodeRel);
	}
}

std::vector<float> Method::updateGraphConnectivity(std::vector<float> nodeRel)
{
	// Init & fill vector with non-visited vertices
	std::vector<float> visited;
	for (unsigned long i = 0; i < nodeRel.size(); i++) {
		visited.push_back(0);
	}
	visited.at(0) = 1; // Stock is always connected

	this->setVisitedNodes(visited);
	this->recursiveVertexVisit(nodeRel);

	visited = this->getVisitedNodes();

	for (unsigned long i = 1; i < visited.size(); i++) {
		if (visited.at(i) != 1) {
			nodeRel.at(i) = 0;
		}
	}

	return nodeRel;
}

void Method::graphToImg(std::string filename, graph_t g)
{
	//write_graphviz (std::cout, g);
	std::ofstream dmp;
	std::string imgPath = "output/graph" + filename + "." + this->_oImgFormat;
	std::string dotPath = "output/graph" + filename + ".dot";
	dmp.open(dotPath);
	write_graphviz(dmp, g, custom_vertex_writer(g, this->_graphModel), custom_edge_writer(g));
	std::string outFormat = "neato";
	std::string gScale = "-n -s" + std::to_string(this->_oImgCoordScale);
	std::string gFormat = "-T" + this->_oImgFormat;
	std::string gSize = "-Gsize="
		+ std::to_string(this->_oImgSizeX) + ","
		+ std::to_string(this->_oImgSizeY) + "!";
	std::string gViewport = "-Gviewport="
		+ std::to_string(this->_oImgSizeX) + ","
		+ std::to_string(this->_oImgSizeY) + ","
		+ std::to_string(this->_oImgCoordScale) + "!";
	std::string gDpi = "-Gdpi=" + std::to_string(this->_accuracy);
	std::string cmd = outFormat +
		" " + gScale +
		" " + dotPath +
		" " + gFormat +
		" " + gSize +
		" " + gViewport +
		" " + gDpi +
		" -o " + imgPath;
	const char* dotCmd = cmd.c_str();

	std::system(dotCmd);
}

/*
 * Returns ratio of black pixels against amount of
 * black pixels for graph whose each node probability is 1
 * */  
float Method::readImg()
{
	std::string imgPath = "output/graph" + std::to_string(this->fileItr) + "." + this->_oImgFormat;

	cv::Mat image;
	image = cv::imread(imgPath, cv::IMREAD_COLOR);

	if (!image.data) std::cout << "Could not open or find the image - " << imgPath;

	// Display img
	/*cv::namedWindow( "Display window", cv::WINDOW_AUTOSIZE );
	cv::imshow( "Display window", image );
	cv::waitKey(0);*/

	// Prepare Image
	cv::cvtColor(image, image, CV_BGR2GRAY);
	cv::threshold(image, image, 254, 255, cv::THRESH_BINARY);
	// Count Pixels
	int count_all = image.cols * image.rows;
	int count_white = cv::countNonZero(image);
	int count_black = count_all - count_white;

	std::cout << "All pixels - " << count_all << "; White pixels - " << count_white
		<< "; Black pixels - " << count_black << std::endl;

	this->fileItr++;

	float square = count_black;
	//square /= this->getMaxCoverage();
	square /= count_all;

	std::cout << "Square - " << square << endl;

	return square;
}

/*
 * FOR PARALLEL METHODS
 * Returns ratio of black pixels against amount of
 * black pixels for graph whose each node probability is 1
 * */
float Method::readImgParallel(unsigned long fileItr)
{
	std::string imgPath = "output/graph" + std::to_string(fileItr) + "." + this->_oImgFormat;

	cv::Mat image;
	image = cv::imread(imgPath, cv::IMREAD_COLOR);

	if (!image.data) std::cout << "Could not open or find the image - " << imgPath;

	// Display img
	/*cv::namedWindow( "Display window", cv::WINDOW_AUTOSIZE );
	cv::imshow( "Display window", image );
	cv::waitKey(0);*/

	// Prepare Image
	cv::cvtColor(image, image, CV_BGR2GRAY);
	cv::threshold(image, image, 254, 255, cv::THRESH_BINARY);
	// Count Pixels
	int count_all = image.cols * image.rows;
	int count_white = cv::countNonZero(image);
	int count_black = count_all - count_white;

	std::cout << "All pixels - " << count_all << "; White pixels - " << count_white
		<< "; Black pixels - " << count_black << std::endl;

	float square = count_black;
	square /= this->getMaxCoverage();;

	std::cout << "Square - " << square << endl;

	return square;
}

/*
 * Returns count of black pixels for all graph, where p of every node is 1
 * */
float Method::maxCoverageReadImg(std::string filename)
{
	std::string imgPath = "output/graph" + filename + "." + this->_oImgFormat;

	cv::Mat image;
	image = cv::imread(imgPath, cv::IMREAD_COLOR);

	if (!image.data) std::cout << "Could not open or find the image - " << imgPath;

	// Prepare Image
	cv::cvtColor(image, image, CV_BGR2GRAY);
	cv::threshold(image, image, 254, 255, cv::THRESH_BINARY);
	// Count Pixels
	int count_all = image.cols * image.rows;
	int count_white = cv::countNonZero(image);
	int count_black = count_all - count_white;

	std::cout << "All pixels - " << count_all << "; White pixels - " << count_white
		<< "; Black pixels - " << count_black << std::endl;

	float square = count_black;
	square /= count_all;
	setMaxCoverageFloat(square);

	return count_black;
}

/*
 * Returns ratio black/all pixel
 * */
float Method::maxCoverageReadImgRatioAgainstAll(std::string filename)
{
	std::string imgPath = "output/graph" + filename + "." + this->_oImgFormat;

	cv::Mat image;
	image = cv::imread(imgPath, cv::IMREAD_COLOR);

	if (!image.data) std::cout << "Could not open or find the image - " << imgPath;

	// Prepare Image
	cv::cvtColor(image, image, CV_BGR2GRAY);
	cv::threshold(image, image, 254, 255, cv::THRESH_BINARY);
	// Count Pixels
	int count_all = image.cols * image.rows;
	int count_white = cv::countNonZero(image);
	int count_black = count_all - count_white;

	std::cout << "All pixels - " << count_all << "; White pixels - " << count_white
		<< "; Black pixels - " << count_black << std::endl;

	float square = count_black;
	square /= count_all;

	std::cout << "Square (against all) - " << square << endl;

	return square;
}

float Method::countSquareTest(std::vector<float> visited)
{
	int sum_square = 0;
	for (int i = 1; i < visited.size(); i++) {
		if (visited.at(i) == 1) sum_square++;
	}
	return sum_square;
}

float Method::countSquareMatrixMaxCoverage(std::vector<float> visited)
{
	bool** matrix = new bool* [this->_oImgSizeX];
	for (int i = 0; i < this->_oImgSizeX; i++)
		matrix[i] = new bool[this->_oImgSizeY];

	int count_all = this->_oImgSizeX * this->_oImgSizeY;
	int count_black = 0;

	// Initialize matrix
	for (unsigned int i = 0; i < this->_oImgSizeX; i++) {
		for (unsigned int j = 0; j < this->_oImgSizeY; j++) {
			matrix[i][j] = false;
		}
	}

	// Draw node circles
	for (int i = 1; i < visited.size(); i++) {
		int x = this->_graphModel.getNodes().at(i).getCoordinates().at(0);
		int y = this->_graphModel.getNodes().at(i).getCoordinates().at(1);
		int radius = this->_graphModel.getNodes().at(i).getCoverage() * this->getAccuracy();
		this->drawCircle(matrix, x, y, radius);
	}

	// Count covered area
	for (unsigned int i = 0; i < this->_oImgSizeX; i++) {
		for (unsigned int j = 0; j < this->_oImgSizeY; j++) {
			if (matrix[i][j]) {
				count_black++;
			}
		}
	}

	// Dealloc memory
	delete[] matrix;

	return count_black;
}

float Method::countSquareMatrixMaxCoverageAgainstAll(std::vector<float> visited)
{
	bool** matrix = new bool* [this->_oImgSizeX];
	for (int i = 0; i < this->_oImgSizeX; i++)
		matrix[i] = new bool[this->_oImgSizeY];

	int count_all = this->_oImgSizeX * this->_oImgSizeY;
	int count_black = 0;

	// Initialize matrix
	for (unsigned int i = 0; i < this->_oImgSizeX; i++) 
	{
		for (unsigned int j = 0; j < this->_oImgSizeY; j++) 
		{
			matrix[i][j] = false;
		}
	}

	// Draw node circles
	for (int i = 1; i < visited.size(); i++) 
	{
		int x = this->_graphModel.getNodes().at(i).getCoordinates().at(0);
		int y = this->_graphModel.getNodes().at(i).getCoordinates().at(1);
		int radius = this->_graphModel.getNodes().at(i).getCoverage() * this->getAccuracy();
		this->drawCircle(matrix, x, y, radius);
	}

	// Count covered area
	for (unsigned int i = 0; i < this->_oImgSizeX; i++) 
	{
		for (unsigned int j = 0; j < this->_oImgSizeY; j++) 
		{
			if (matrix[i][j]) {
				count_black++;
			}
		}
	}

	// Dealloc memory
	delete[] matrix;

	float square = count_black;
	square /= count_all;

	return square;
}

float Method::countSquareMatrix(std::vector<float> visited)
{
	bool** matrix = new bool* [this->_oImgSizeX];
	for (int i = 0; i < this->_oImgSizeX; i++)
		matrix[i] = new bool[this->_oImgSizeY];

	int count_black = 0;

	// Initialize matrix
	for (unsigned int i = 0; i < this->_oImgSizeX; i++) 
	{
		for (unsigned int j = 0; j < this->_oImgSizeY; j++) 
		{
			matrix[i][j] = false;
		}
	}

	// Draw node circles
	for (int i = 1; i < visited.size(); i++) 
	{
		int x = this->_graphModel.getNodes().at(i).getCoordinates().at(0);
		int y = this->_graphModel.getNodes().at(i).getCoordinates().at(1);
		int radius = this->_graphModel.getNodes().at(i).getCoverage() * this->getAccuracy();
		this->drawCircle(matrix, x, y, radius);
	}

	// Count covered area
	for (unsigned int i = 0; i < this->_oImgSizeX; i++) 
	{
		for (unsigned int j = 0; j < this->_oImgSizeY; j++) 
		{
			if (matrix[i][j]) 
			{
				count_black++;
			}
		}
	}

	// Debug Code
	/*for(int i = 0; i < this->_oImgSizeX; i++) {
		for (int j = 0; j < this->_oImgSizeY; j++)
			if (matrix[i][j]) { std::cout << "*"; } else std::cout << " ";
		std::cout << "\n";
	}
	std::cout << "\n --------------- \n";*/

	// Dealloc memory
	delete[] matrix;

	float square = count_black;
	square /= this->getMaxCoverageMatrix();

	return square;
}

// Help function countSquareMatrix method
void Method::drawCircle(bool** matrix, int x0, int y0, int radius)
{
	int x = radius;
	int y = 0;
	int xChange = 1 - (radius << 1);
	int yChange = 0;
	int radiusError = 0;

	while (x >= y)
	{
		for (int i = x0 - x; i <= x0 + x; i++)
		{
			if ((i >= 0) && (i < this->_oImgSizeX)) {
				int yNew = y0 + y;
				if ((yNew >= 0) && (yNew < this->_oImgSizeY)) matrix[i][yNew] = true;
				yNew = y0 - y;
				if ((yNew >= 0) && (yNew < this->_oImgSizeY)) matrix[i][yNew] = true;
			}
		}
		for (int i = x0 - y; i <= x0 + y; i++)
		{
			if ((i >= 0) && (i < this->_oImgSizeX)) {
				int yNew = y0 + x;
				if ((yNew >= 0) && (yNew < this->_oImgSizeY)) matrix[i][yNew] = true;
				yNew = y0 - x;
				if ((yNew >= 0) && (yNew < this->_oImgSizeY)) matrix[i][yNew] = true;
			}
		}

		y++;
		radiusError += yChange;
		yChange += 2;
		if (((radiusError << 1) + xChange) > 0)
		{
			x--;
			radiusError += xChange;
			xChange += 2;
		}
	}
}

// Simple draw circle, less accurate not suggested for usage
void Method::drawCircleSimple(bool** matrix, int x0, int y0, int r)
{
	for (int i = x0 - r; i <= x0 + r; i++)
	{
		for (int j = y0 - r; j <= y0 + r; j++)
		{
			if (((i - x0) * (i - x0) + (j - y0) * (j - y0)) <= r * r)
			{
				if (((i >= 0) && (i < this->_oImgSizeX)) && ((j >= 0) && (j < this->_oImgSizeY)))
					matrix[i][j] = true;
			}
		}
	}
}

graph_t Method::genNewGraph(std::vector<float> visited)
{
	Graph graphModel = this->getGraphModel();
	std::vector<Edge> edgeVec;
	unsigned long verticesNum = 0;
	std::cout << "Adding edges to graph - END" << std::endl;
	for (unsigned long i = 0; i < visited.size(); i++) {
		if (visited.at(i) == 1) {
			for (unsigned int neighborVertex : graphModel.getNodes().at(i).getRelations()) {
				for (unsigned long j = 0; j < visited.size(); j++) {
					if ((j == neighborVertex) && (visited.at(j) == 1)) {
						edgeVec.push_back(Edge(i, neighborVertex));
					}
				}
			}
			verticesNum++;
		}
	}
	std::cout << "Adding edges to graph - END" << std::endl;

	std::cout << "Initializing graph with edges - START" << std::endl;
	// GraphInit
	graph_t g(edgeVec.begin(), edgeVec.end(), verticesNum);
	std::cout << "Initializing graph with edges - END" << std::endl;

	return g;
}

float Method::countSquare(std::vector<float> visited)
{
	graph_t g = genNewGraph(visited);
	this->graphToImg(std::to_string(this->fileItr), g);
	return this->readImg();
}

//FOR PARALLEL METHODS
float Method::countSquareParallel(std::vector<float> visited, unsigned long fileItr)
{
	graph_t g = genNewGraph(visited);
#pragma omp critical
	{
		this->graphToImg(std::to_string(fileItr), g);
	}
	return this->readImgParallel(fileItr);
}

void Method::boost_bfs()
{
	graph_t g = this->getUndirectedGraph();
	custom_bfs_visitor vis;
	breadth_first_search(g, vertex(this->getGraphModel().getStockId(), g), visitor(vis));
}

void Method::boost_dfs()
{
	graph_t g = this->getUndirectedGraph();
	auto indexmap = boost::get(boost::vertex_index, g);
	auto colormap = boost::make_vector_property_map<boost::default_color_type>(indexmap);

	custom_dfs_visitor vis;
	depth_first_search(g, visitor(vis));
}