#pragma once

#include "boost/graph/graph_traits.hpp"
#include "boost/graph/adjacency_list.hpp"
#include "boost/graph/depth_first_search.hpp"
#include "boost/graph/breadth_first_search.hpp"

#include "../../Models/Graph.h"

extern std::string INPUT_FILE_PATH;//("input/graph_input.json");

using namespace boost;

typedef std::pair<int, int> Edge;
typedef adjacency_list<vecS, vecS, undirectedS,
	property<edge_color_t, default_color_type> > graph_t;
typedef graph_traits<graph_t>::vertex_descriptor vertex_t;

class custom_bfs_visitor : public default_bfs_visitor
{
public:
	template <typename Vertex, typename Graph> void discover_vertex(Vertex v, const Graph& g);
};

class custom_dfs_visitor : public default_dfs_visitor
{
public:
	void discover_vertex(vertex_t v, const graph_t& g);
};

// Graphviz property writer (vertex)
class custom_vertex_writer
{
public:
	custom_vertex_writer(graph_t& g, Graph& gModel);

	template <class VertexOrEdge> void operator()(std::ostream& out, const VertexOrEdge& e);
private:
	graph_t& currentGraph;
	Graph& currentGraphModel;
};

// Graphviz property writer (edge)
class custom_edge_writer
{
public:
	custom_edge_writer(graph_t& g);

	template <class VertexOrEdge> void operator()(std::ostream& out, const VertexOrEdge& e);
private:
	graph_t& currentGraph;
};

class Method
{
public:
	Method(unsigned int accuracy, unsigned int oImgSizeX, unsigned int oImgSizeY, unsigned int oImgScale, string oImgFormat, float reliabilityFlag);

	void init();

	unsigned int getAccuracy();

	void setAccuracy(unsigned int accuracy);

	std::vector<Edge> getEdgeVector();

	void setEdgeVector(std::vector<Edge> edgeVector);

	graph_t getUndirectedGraph();

	void setUndirectedGraph(graph_t undGraph);

	Graph getGraphModel();

	void setGraphModel(Graph graphModel);

	unsigned long getMaxCoverage();

	float getMaxCoverageFloat();

	bool getReliabilityFlag();

	void setReliabilityFlag(bool value);

	void setMaxCoverageFloat(float maxCoverage);

	void setMaxCoverage(unsigned long maxCoverage);

	unsigned long getMaxCoverageMatrix();

	float getSquareMatrixMaxCoverageAgainstAll();

	void setSquareMatrixMaxCoverageAgainstAll(float value);

	void setMaxCoverageMatrix(unsigned long maxCoverageMatrix);
protected:
	std::vector<float> getVisitedNodes();

	void setVisitedNodes(std::vector<float> visited);

	void graphInit(Graph graphModel);

	void setMaxCoverageInit();

	float setThreshold();

	void setThreshold(float);

	float getThreshold();

	std::vector<float> getGraphProbabilities();
	// Return set of visited vertices in connected graph
	void recursiveVertexVisit(std::vector<float> nodeRel);

	std::vector<float> updateGraphConnectivity(std::vector<float> nodeRel);

	void graphToImg(std::string filename, graph_t g);

	/*
	 * Returns ratio of black pixels against amount of
	 * black pixels for graph whose each node probability is 1
	 * */
	float readImg();

	/*
	 * FOR PARALLEL METHODS
	 * Returns ratio of black pixels against amount of
	 * black pixels for graph whose each node probability is 1
	 * */
	float readImgParallel(unsigned long fileItr);

	/*
	 * Returns count of black pixels for all graph, where p of every node is 1
	 * */
	float maxCoverageReadImg(std::string filename);

	/*
	 * Returns ratio black/all pixel
	 * */
	float maxCoverageReadImgRatioAgainstAll(std::string filename);

	float countSquareTest(std::vector<float> visited);

	float countSquareMatrixMaxCoverage(std::vector<float> visited);

	float countSquareMatrixMaxCoverageAgainstAll(std::vector<float> visited);

	float countSquareMatrix(std::vector<float> visited);

	// Help function countSquareMatrix method
	void drawCircle(bool** matrix, int x0, int y0, int radius);

	// Simple draw circle, less accurate not suggested for usage
	void drawCircleSimple(bool** matrix, int x0, int y0, int r);

	graph_t genNewGraph(std::vector<float> visited);

	float countSquare(std::vector<float> visited);

	//FOR PARALLEL METHODS
	float countSquareParallel(std::vector<float> visited, unsigned long fileItr);

	void boost_bfs();

	void boost_dfs();

	// Output image properties
	unsigned int _accuracy;
	unsigned int _oImgSizeX;
	unsigned int _oImgSizeY;
	unsigned int _oImgCoordScale;
	std::string  _oImgFormat;
	// Boost graph entities
	std::vector<Edge>  _edgeVector;
	graph_t _graph_t;
	unsigned long _maxCoverage;
	float _maxCoverageFloat;
	unsigned long _maxCoverageMatrix;
	int i;
	float threshold;
	bool reliabilityFlag;
	float squareMatrixMaxCoverageAgainstAll;
	// Graph inited model
	Graph   _graphModel;
	// Img
	unsigned long fileItr;
	// Visited nodes
	std::vector<float> _visited;
};